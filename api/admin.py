from django.contrib import admin
from api import models


@admin.register(models.Supply)
class SupplyAdmin(admin.ModelAdmin):
    list_display = ('id', 'km_supply', 'quantity_liters', 'value', 'date_supply', 'created_at')
    search_fields = ('id', 'km_supply', 'quantity_liters', 'value', 'date_supply', 'created_at')

    ordering = ['-id']
    list_per_page = 10
